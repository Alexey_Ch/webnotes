angular.module('myApp', []).
    service('profileService', function() {
        var profile = {name:'', email:''};
        this.getProfile = function() {
            return this.profile;
        }
        this.setProfile = function(profile) {
            this.profile = profile;
        }
    }).
    service('notesService', function () {
        var data = [
            {id:1, title:'Note 1', text:'Note text'},
            {id:2, title:'Note 2', text:'Note text'},
            {id:3, title:'Note 3', text:'Note text'},
            {id:4, title:'Note 4', text:'Note text'},
            {id:5, title:'Note 5', text:'Note text'},
            {id:6, title:'Note 6', text:'Note text'},
            {id:7, title:'Note 7', text:'Note text'},
            {id:8, title:'Note 8', text:'Note text'}
        ];

        this.notes = function () {
            return data;
        }
        this.getEmptyIndex = function() {
            var ids = [];
            angular.forEach(data, function (note) {
                ids.push(note.id);
            });
            ids.sort();
            return ids[ids.length - 1] + 1;
        }
        this.addNote = function (note) {
            if(note.id == -1) {
                var currentIndex = this.getEmptyIndex();
                note.id = currentIndex;
            }
            data.push(note);
        }
        this.getNote = function (id) {
            var neededNote = {};
            angular.forEach(data, function (note) {
                if (note.id == id){
                    neededNote = note;
                    return;
                }
            });
            return neededNote;
        }
        this.deleteNote = function (id) {
            var oldNotes = data;
            data = [];
            angular.forEach(oldNotes, function (note) {
                if (note.id !== id) data.push(note);
            });
        }
    })
    .directive('myNotebook', function (/*$log*/) {
        return {
            restrict:"E",
            scope:{
                notes:'='/*,
                ondelete:'&'*/
            },
            templateUrl:"partials/notebook.html",
            controller:'NotebookCtrl'
        };
    })
    .controller('NotebookCtrl', ['$scope', 'notesService', function ($scope, notesService) {
    $scope.getNotes = function () {
        return notesService.notes();
    };
    $scope.addNote = function (note) {
        note.id = -1;
        if(note.title != '') {
            notesService.addNote(note);
        }
    };

    $scope.deleteNote = function (id) {
        notesService.deleteNote(id);
    };

    $scope.resetForm = function() {
        $scope.noteTitle = '';
    };

    $scope.notes = notesService.notes();
    $scope.note = {};
}]).
    config(['$routeProvider', function($routeProvider) {
        $routeProvider.
            when('/new', {templateUrl: 'partials/edit.html', controller: 'NotebookCtrl'}).
            when('/edit/:id', {templateUrl: 'partials/edit.html', controller: 'EditController'}).
            when('/profile', {templateUrl: 'partials/profile.html', controller: 'ProfileController'}).
            when('/', {templateUrl: 'partials/list.html'}).
            otherwise('/')
    }]);